package ifpb.pp.modelo;

import java.util.Iterator;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author Ricardo Job
 */
public class DataSourceAluno implements JRDataSource {

    private Iterator<Aluno> alunos;
    private Aluno cursor;

    public DataSourceAluno() {
    }

    public DataSourceAluno(List<Aluno> alunos) {
        this.alunos = alunos.iterator();
    }

    public boolean next() throws JRException {
        boolean retorno = alunos.hasNext();

        if (retorno) {
            cursor = alunos.next();
        }
        return retorno;
    }

    public Object getFieldValue(JRField campo) throws JRException {


        if (null != campo.getName())//        Aluno cursor = cursor;
        switch (campo.getName()) {
            case "nome":
                return cursor.getNome();
            case "cpf":
                return cursor.getCpf();
            case "idade":
                return cursor.getIdade();
            case "disciplinas":
                return new JRBeanCollectionDataSource(cursor.getDisciplinas(), false);
        }
        return "";
    }
}
