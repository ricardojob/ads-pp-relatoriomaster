package ifpb.pp.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ricardo Job
 */
public class Aluno {

    private String nome;
    private String cpf;
    private int idade;

    private List<Disciplina> disciplinas;

    public Aluno() {
        disciplinas=new ArrayList<>();
    }

    public Aluno(String nome, String cpf, int idade) {
        this();
        this.nome = nome;
        this.cpf = cpf;
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void addDisciplina(String nome, int creditos) {
        addDisciplina(new Disciplina(nome, creditos));
    }

    public void addDisciplina(Disciplina disciplina) {
        this.disciplinas.add(disciplina);
    }

}
