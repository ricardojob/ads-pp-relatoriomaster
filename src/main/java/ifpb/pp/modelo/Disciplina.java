package ifpb.pp.modelo;

/**
 * @author Ricardo Job
 */
public class Disciplina {

    private String descricao;
    private int creditos;

    public Disciplina() {
    }

    public Disciplina(String nome, int creditos) {
        this.descricao = nome;
        this.creditos = creditos;
    }

    public String getNome() {
        return descricao;
    }

    public void setNome(String nome) {
        this.descricao = nome;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

}
