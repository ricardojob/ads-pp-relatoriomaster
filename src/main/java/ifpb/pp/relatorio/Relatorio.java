package ifpb.pp.relatorio;

import ifpb.pp.modelo.Aluno;
import ifpb.pp.modelo.DataSourceAluno;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * @author Ricardo Job
 */
public class Relatorio {

    private InputStream canal;
    private Map map;
    private List dados;

    public Relatorio() {
        this.map = new HashMap();
    }

    public byte[] bytes() {
        try {
            JasperReport relatorio = (JasperReport) JRLoader.loadObject(canal);
//            JasperPrint impressao = new JasperPrint();
            DataSourceAluno source = new DataSourceAluno(dados);
//            impressao = JasperFillManager.fillReport(relatorio, map, source);
            JasperPrint impressao = JasperFillManager.fillReport(relatorio, map, source);
            return JasperExportManager.exportReportToPdf(impressao);
        } catch (JRException ex) {
            Logger.getLogger(Relatorio.class.getName()).log(Level.SEVERE, null, ex);
            return new byte[0];
        }
    }     

    public void input(InputStream jp) {
        this.canal = jp;
    }

    public void dados(List list) {
        this.dados = list;
    }

    public void addParametro(String chave, Object valor) {
        this.map.put(chave, valor);
    }

    public Map getMap() {
        return map;
    }
}
