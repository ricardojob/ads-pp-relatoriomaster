package ifpb.pp.relatorio;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * @author job
 */
@Named
@ApplicationScoped
public class UtilJSF {

    @Produces
    public ExternalContext produzir() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getExternalContext();
    }

    @Produces
    public FacesContext produzirFaces() {
        return FacesContext.getCurrentInstance();

    }
}
