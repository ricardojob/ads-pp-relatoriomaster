package ifpb.pp.relatorio;

import ifpb.pp.modelo.Aluno;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author job
 */
@Named(value = "controladorJSF")
@RequestScoped
public class ControladorJSF {

    @Inject
    private ExternalContext externalContext;

    @Inject
    private FacesContext facesContext;
    private String DIR = "/WEB-INF/relatorios/";

    public ControladorJSF() {
    }

    public void download(ActionEvent e) {
        try {
            Relatorio relatorio = new Relatorio();
            relatorio.input(input());
            relatorio.addParametro("titulo", "Relatorio de Alunos");
            relatorio.addParametro("subreport_dir", getDir());
            relatorio.dados(listarAlunos());
            realizarDownload(relatorio);
        } catch (IOException ex) {
            Logger.getLogger(ControladorJSF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void realizarDownload(Relatorio relatorio) throws IOException {
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        response.reset();
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition",
                "attachment; filename=\"name.pdf\"");

        try (OutputStream output = response.getOutputStream()) {
            output.write(relatorio.bytes());
            output.flush();
        }

        facesContext.responseComplete();
    }

    private static List<Aluno> listarAlunos() {
        List<Aluno> alunos = new ArrayList<>();

        Aluno kiko = new Aluno("Kiko", "1235", 14);
        kiko.addDisciplina("DAC", 6);
        kiko.addDisciplina("Práticas", 5);
        Aluno chaves = new Aluno("Chaves", "1234", 12);
        chaves.addDisciplina("PSD", 5);
        chaves.addDisciplina("Padrões de Projeto", 5);
        Aluno florinda = new Aluno("Dona Clotilde", "1236", 71);
        florinda.addDisciplina("POS", 4);
        florinda.addDisciplina("PDM", 5);

        alunos.add(kiko);
        alunos.add(chaves);
        alunos.add(florinda);

        alunos.add(new Aluno("Madruga, Sr.", "1238", 50));

        return alunos;
    }

    private InputStream input() {
        String path = DIR + "ExemploDataSource.jasper";
        InputStream jp = externalContext.getResourceAsStream(path);
        return jp;
    }

    private String getDir() throws MalformedURLException {
//        String caminho = externalContext.getResource(DIR).getPath();
        String caminho = externalContext.getRealPath(DIR)+"\\";
        return caminho;
    }

}
